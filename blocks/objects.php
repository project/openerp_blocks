<?php


/**
 * @file
 * Example of the simple usage of the OpenERP Blocks module. It lists all available
 * OpenERP objects in the database and render all fields in a table.
 *
 * copyright (c) 2010, Pinacono Co., Ltd.
 *
 * This is a very simple example for the OpenERP block view. Developer can use the
 * full PHP capability to retrieve data from OpenERP (using OpenERP module's API),
 * process the data, and render the output using any required method, including
 * the bare PHP formatting, any other template engine, or the Drupal theming.
 *
 * Following is the programmatic resources those available in this file:
 *
 * - Drupal API is fully available, including the form API, theme functions, and other
 *   Drupal's API.
 *
 * - The '$block' variable is loaded with the basic block information, including:
 *   $block->id : block delta id
 *   $block->subject : block subject to be rendered on block (default is the base name)
 *   $block->name : name of this file without directory and extension
 *   $block->basename : name of this file without directory
 *   $block->filename : full file name of this file.
 *
 * - OpenERP API by OpenERP module is available - please check the file openerp_api.inc
 *   for details.
 *
 * - The OpenERP authentication information is coming from the user's settings. However,
 *   default auth info is available as the administration settings.
 *
 * - This page can be called by either the hook_block('view'), or by the menu path
 *   openerp/block/<name>/... Therefore, the developer may check the origin of accessing
 *   by investigate the result of the arg() function.
 */

$offset = 0;
$limit  = 10;

if ( ($pager = arg(3)) != NULL ) {
  list($offset, $limit) = explode(',', $pager);
}

// set block subject alternatively
if ( isset($block) ) {
  $block->subject = '10 First OpenERP Objects';
}
?>
<div class="ui-widgets ui-corner-all">
<?php
// make request
$proxy = OpenERPProxy::getProxy();

if ( ($r = $proxy->page('ir.model', array(), (int) $offset, (int) $limit)) === FALSE ) {
  // error, print error message
?>
  <div class="error message"><?php echo OpenERPProxy::message(); ?></div>
<?php
}
else {
  // render table
?>
<table id="openerp-<?php echo $delta; ?>">
<tr>
<?php
  foreach ( $r[0] as $name => $val ) {
    echo "<th>$name</th>";
  }
?>
</tr>
<?php
  // data
  foreach ( $r as $data ) {
    echo '<tr>';
    foreach ( $data as $name => $val ) {
      echo '<td>'. ( is_array($val) ? implode(', ', $val) :  $val ) .'</td>';
    }
    echo '</tr>';
  }
?>
</table>
<?php
}
?>
</div>